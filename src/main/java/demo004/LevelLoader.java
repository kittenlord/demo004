package demo004;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import demo004.config.GameObjectConfig;

/**
 * Loads the level contents from a text file.
 */
public class LevelLoader {

	private final Map<String, GameObjectConfig> objectByMapCode;

	/**
	 * Create a new level loader that can load given objects.
	 */
	public LevelLoader(List<GameObjectConfig> allObjects) {
		objectByMapCode = new HashMap<>();
		for (var object : allObjects) {
			objectByMapCode.put(object.mapCode(), object);
		}
	}

	/**
	 * Returns all objects in level in their positions.
	 */
	public Map<Position, List<GameObjectConfig>> loadLevel(String levelFileName) throws IOException {
		return loadLevel(Resources.getText(levelFileName));
	}

	/**
	 * Parses the level data. Format of the level is the following:
	 * <p>
	 * Each object is represented by a code, defined in the game configuration file,
	 * consisting of one or more characters. One object's code should not be a
	 * prefix of another object's code (such as "a" and "ab").
	 * <p>
	 * Multiple objects at the same position are concatenated, starting with the one
	 * at the bottom. For example, if there is an object "a", and on top of it an
	 * object "b1", write "ab1". Thus, a sequence of characters represents a pile of
	 * objects (that is, one or more objects at the same position).
	 * <p>
	 * Positions with the same Y coordinate are written on the same line, separated
	 * by one or more spaces. For example, if there is an object "a" on the left,
	 * and objects "a" and "b1" on the right, write "a ab1". Positions with
	 * different Y coordinates are written on different lines.
	 * <p>
	 * Piles of objects with the same X coordinate must be vertically aligned in the
	 * text file, using as many spaces as necessary. For example, if the first line
	 * is "a ab1" and the second line is "ab1 a", the first "a" in the first line
	 * must be followed by three spaces (like this: "a---ab1", "ab1-a"), so that the
	 * heaps for the second column are also vertically aligned. In other words,
	 * different columns on the screen must be separated by a column of spaces in
	 * the text file.
	 */
	Map<Position, List<GameObjectConfig>> loadLevel(List<String> lines) {
		Map<Position, List<GameObjectConfig>> result = new HashMap<>();

		// the yet unparsed characters in each line (initialized to: none)
		List<String> unparsedByLine = lines.stream().map(s -> "").collect(Collectors.toList());

		// did the previous column contain only spaces?
		boolean previousOnlySpaces = true;

		// we are currently doing this X coordinate in the level
		int levelX = 0;

		// outer loop: the X coordinate in the text file
		for (int cursorX = 0;; cursorX++) {

			// are all lines shorter than cursorX? (yes, unless proved otherwise)
			boolean beyondAllLines = true;

			// does this column contain only spaces? (yes, unless proved otherwise)
			boolean onlySpaces = true;

			// inner loop: the Y coordinate in the text file, i.e. the line of the text file
			for (int levelY = 0; levelY < lines.size(); levelY++) {
				String line = lines.get(levelY);
				// restore the yet unparsed characters
				String unparsed = unparsedByLine.get(levelY);

				// is this line shorter than cursorX?
				if (line.length() <= cursorX) {
					// do we still have unparsed characters at the end of this line?
					if (!unparsed.isEmpty()) {
						throw new IllegalStateException("Can't decode map: " + unparsed);
					}
					continue;
				} else {
					beyondAllLines = false;
				}

				// does this line contain a space at this position?
				char c = line.charAt(cursorX);
				if (' ' == c) {
					if (!unparsed.isEmpty()) {
						// do we still have unparsed characters before the space?
						throw new IllegalStateException("Can't decode map: " + unparsed);
					}
				} else {
					onlySpaces = false;
					unparsed = unparsed + c;
					var object = objectByMapCode.get(unparsed);
					if (null != object) {
						unparsed = "";
						var pos = new Position(levelX, levelY);
						var list = result.get(pos);
						if (null == list) {
							list = new ArrayList<>();
							result.put(pos, list);
						}
						list.add(object);
					}
				}
				// store the unparsed characters
				unparsedByLine.set(levelY, unparsed);
			}
			if (beyondAllLines) {
				break;
			}

			if (onlySpaces && !previousOnlySpaces) {
				levelX++;
			}
			previousOnlySpaces = onlySpaces;
		}
		return result;
	}

}
