package demo004;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

/**
 * Static methods for accessing the game resources.
 */
public class Resources {

	private static final String CONFIG_FILE_NAME = "config.properties";

	private static final int WHITE = 0xff_ff_ff_ff;
	private static final int TRANSPARENT = 0x00_ff_ff_ff;

	/**
	 * Returns the properties of the game configuration file.
	 */
	public static Properties getConfigProperties() throws IOException {
		Properties props = new Properties();
		props.load(Resources.class.getResourceAsStream(CONFIG_FILE_NAME));
		return props;
	}

	/**
	 * Loads an image.
	 */
	public static BufferedImage getImage(String imageFileName) {
		try (var is = Resources.class.getResourceAsStream(imageFileName)) {
			if (null == is) {
				throw new IllegalStateException(String.format("Resource '%s' not found", imageFileName));
			}
			return ImageIO.read(is);
		} catch (IOException e) {
			throw new IllegalStateException(String.format("Error loading resource '%s'", imageFileName), e);
		}
	}

	/**
	 * Loads the contents of a text file.
	 */
	public static List<String> getText(String fileName) throws IOException {
		try (var is = Resources.class.getResourceAsStream(fileName);
				var isr = new InputStreamReader(is);
				var reader = new BufferedReader(isr); //
		) {
			return reader.lines().collect(Collectors.toList());
		}
	}

	/**
	 * Converts white pixels to transparent.
	 */
	public static BufferedImage whiteToTransparent(BufferedImage image) {
		int width = image.getWidth();
		int height = image.getHeight();
		BufferedImage copy = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				int color = image.getRGB(x, y);
				copy.setRGB(x, y, (WHITE == color) ? TRANSPARENT : color);
			}
		}
		return copy;
	}
	
}
