package demo004;

import java.io.IOException;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import demo004.config.Config;
import demo004.util.ConfigLoader;

public class Main {

	/**
	 * Entry point of the game.
	 */
	public static void main(String[] args) {
		try {
			var config = new ConfigLoader(Resources.getConfigProperties()).load(Config.class);
			SwingUtilities.invokeLater(() -> new GameDialog(config).create());
		} catch (IOException | IllegalArgumentException e) {
			JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}

}
