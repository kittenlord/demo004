package demo004.util;

import java.lang.reflect.Constructor;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.RecordComponent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class ConfigLoader {

	private final ConfigurationData data;

	public ConfigLoader(Properties properties) {
		data = new ConfigurationData(properties);
	}

	/**
	 * Returns an instance of given record type.
	 */
	public <T> T load(Class<T> configClass) {
		return load("", configClass);
	}

	/**
	 * Returns an instance of given record type.
	 * <p>
	 * If the prefix argument is "p.", key "p.a" contains property "a".
	 */
	public <T> T load(String prefix, Class<T> configClass) {
		if (!configClass.isRecord()) {
			throw new IllegalArgumentException("Class " + configClass.getName() + " is not a record");
		}
		@SuppressWarnings("unchecked")
		Constructor<T> cons = (Constructor<T>) configClass.getConstructors()[0];
		List<Object> values = new ArrayList<>();
		for (var rc : configClass.getRecordComponents()) {
			values.add(loadValue(prefix, rc));
		}
		try {
			return cons.newInstance(values.toArray());
		} catch (ReflectiveOperationException e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * Returns a list of instances of given record type. The instances are ordered
	 * alphabetically by their keys.
	 * <p>
	 * If the key argument is "p", keys "p.k.*" contain an instance.
	 * 
	 */
	private <T> List<T> loadRecordList(String key, Class<T> elementConfigClass) {
		List<T> list = data.getSubKeysAsStream(key) //
				.map(k -> load(key + "." + k + ".", elementConfigClass)) //
				.collect(Collectors.toList());
		return Collections.unmodifiableList(list);
	}

	private Object loadValue(String prefix, RecordComponent rc) {
		String name = rc.getName();
		String key = prefix + name;
		var type = rc.getType();
		if (List.class == type) {
			var pType = (ParameterizedType) rc.getGenericType();
			var argumentType = (Class<?>) pType.getActualTypeArguments()[0];
			if (!Record.class.isAssignableFrom(argumentType)) {
				throw new IllegalArgumentException("Only lists of records are supported, not lists of: " + argumentType);
			}
			return loadRecordList(key, argumentType);
		} else if (Integer.TYPE == type) {
			return data.getInt(key);
		} else if (Boolean.TYPE == type) {
			return data.getBool(key);
		} else if (String.class == type) {
			return data.getString(key);
		} else {
			throw new IllegalArgumentException("Unsupported type: " + type);
		}
	}

}
