package demo004.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.stream.Stream;

/**
 * Returns values of different types from the configuration property file.
 */
public class ConfigurationData {

	/**
	 * These values (case insensitive) are interpreted as "true" when reading a
	 * boolean.
	 */
	private static final List<String> TRUE_VALUES = Collections.unmodifiableList(Arrays.asList( //
			"true", "yes", "on", "1"));

	/**
	 * These values (case insensitive) are interpreted as "false" when reading a
	 * boolean.
	 */
	private static final List<String> FALSE_VALUES = Collections.unmodifiableList(Arrays.asList( //
			"false", "no", "off", "0"));

	private final Properties props;

	public ConfigurationData(Properties properties) {
		this.props = properties;
	}

	/**
	 * Returns a string value for the given key, or null if there is no such key.
	 */
	public String getString(String key) {
		return props.getProperty(key);
	}

	/**
	 * Returns an int value for the given key. Throws a RuntimeException if there is
	 * no such key, or if the value cannot be interpreted as an integer.
	 */
	public int getInt(String key) {
		String s = props.getProperty(key);
		if (null == s) {
			throw new IllegalArgumentException("Config key " + key + " not found");
		}
		return Integer.decode(s);
	}

	/**
	 * Returns a boolean value for the given key, false if there is no such key.
	 * Throws a RuntimeException if the key is present, but the value cannot be
	 * interpreted as either true or false.
	 */
	public boolean getBool(String key) {
		String s = props.getProperty(key);
		if (null == s) {
			return false;
		}
		String low = s.toLowerCase();
		if (TRUE_VALUES.contains(low)) {
			return true;
		}
		if (FALSE_VALUES.contains(low)) {
			return false;
		}
		throw new IllegalArgumentException("Config key " + key + " does not contain a boolean value: " + s);
	}

	/**
	 * Returns an alphabetically ordered list of possible subkeys following the
	 * parent key. If the parent key is "a.b" and there is a key "a.b.c" or
	 * "a.b.c.d", the result list will contain "c".
	 */
	public Stream<String> getSubKeysAsStream(String parentKey) {
		String prefix = parentKey + ".";
		return props.keySet().stream() //
				.filter(o -> o instanceof String).map(o -> (String) o) //
				.filter(s -> s.startsWith(prefix)).map(s -> s.substring(prefix.length())) //
				.map(ConfigurationData::beforeFirstDot) //
				.sorted().distinct();
	}

	/**
	 * Returns the part of the string before the first dot; or the entire string if
	 * there is no dot.
	 */
	static String beforeFirstDot(String s) {
		int p = s.indexOf(".");
		return (p < 0) ? s : s.substring(0, p);
	}

}
