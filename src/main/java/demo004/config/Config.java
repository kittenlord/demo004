package demo004.config;

import java.util.List;

/**
 * The game configuration.
 */
public record Config( //
		int windowWidth, //
		int windowHeight, //
		String windowName, //
		String windowIcon, //
		String titleImage, //
		int objectWidth, //
		int objectHeight, //
		List<GameObjectConfig> objects, //
		List<LevelConfig> levels //
) {
	//
}
