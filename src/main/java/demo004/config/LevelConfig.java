package demo004.config;

/**
 * Level configuration.
 */
public record LevelConfig( //
		String fileName, //
		String backgroundColor //
) {
	//
}
