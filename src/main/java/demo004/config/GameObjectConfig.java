package demo004.config;

/**
 * Game object configuration.
 */
public record GameObjectConfig( //
		String mapCode, //
		String image, //
		boolean isSolid, //
		boolean isPlayer, //
		boolean isExit, //
		String lockCode, //
		String keyCode //
) {
	//
}
