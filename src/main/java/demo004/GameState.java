package demo004;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import demo004.config.Config;
import demo004.config.GameObjectConfig;

/**
 * Current state of the game: level dimensions, objects at each position, which
 * locks are open.
 */
public class GameState {

	private final Config cfg;
	private Map<Position, List<GameObjectConfig>> objects;
	private int levelWidth;
	private int levelHeight;
	private Set<String> openLocks;

	public GameState(Config config) {
		this.cfg = config;
	}

	public Integer currentLevel = null;

	/**
	 * Load the starting position of given level, replacing the current one.
	 */
	public void initLevel(int level) throws IOException {
		currentLevel = level;
		var loader = new LevelLoader(cfg.objects());
		objects = loader.loadLevel(cfg.levels().get(level).fileName());
		levelWidth = objects.keySet().stream().mapToInt(Position::x).max().orElse(-1) + 1;
		levelHeight = objects.keySet().stream().mapToInt(Position::y).max().orElse(-1) + 1;
		openLocks = new HashSet<>();
	}

	/**
	 * Switch to the next level, or to the title screen if this was the last level.
	 */
	public void nextLevel() throws IOException {
		int nextLevel = currentLevel + 1;
		if (nextLevel < cfg.levels().size()) {
			initLevel(nextLevel);
		} else {
			currentLevel = null;
		}
	}

	/**
	 * Move the player in given direction. Return true if something happened, false
	 * otherwise.
	 */
	public boolean movePlayer(Direction direction) {
		Optional<Position> posOpt = getPlayerPosition();
		if (posOpt.isEmpty()) {
			return false; // nothing to move
		}
		var pos = posOpt.get();
		var pos2 = direction.nextPosition(pos);
		if (!canMoveTo(pos2)) {
			return false; // cannot move there
		}
		var player = objects.get(pos).stream() //
				.filter(GameObjectConfig::isPlayer).findAny().get();
		objects.get(pos).remove(player);
		objects.get(pos2).add(player);
		var collectibles = objects.get(pos2).stream() //
				.filter(o -> null != o.keyCode()) //
				.collect(Collectors.toList());
		for (var obj : collectibles) {
			openLocks.add(obj.keyCode());
			objects.get(pos2).remove(obj);
		}
		return true;
	}

	/**
	 * Returns the player position; or nothing if there is no player. There should
	 * always be a player in the level, but this was we avoid throwing an exception
	 * if we forgot to add one.
	 */
	private Optional<Position> getPlayerPosition() {
		return objects.entrySet().stream() //
				.filter(e -> e.getValue().stream().anyMatch(GameObjectConfig::isPlayer)) //
				.map(e -> e.getKey()) //
				.findAny();
	}

	/**
	 * Returns whether player can move to given position. The player cannot move to
	 * a position if one of the objects there prevents that (a wall, or a locked
	 * door), or if the position is outside the level.
	 */
	private boolean canMoveTo(Position pos) {
		var objs = objects.get(pos);
		if (null == objs) {
			return false; // cannot move outside of game plan
		}
		return objs.stream().allMatch(this::canMoveTo);
	}

	/**
	 * Whether this object is a wall or a locked door, preventing the player from
	 * moving to its position.
	 */
	private boolean canMoveTo(GameObjectConfig obj) {
		var lock = obj.lockCode();
		boolean locked = (null != lock) && !openLocks.contains(lock);
		return !(obj.isSolid() || locked);
	}

	/**
	 * Whether the level is completed; that is, whether the player is at an exit.
	 */
	public boolean isLevelCompleted() {
		Optional<Position> posOpt = getPlayerPosition();
		if (posOpt.isEmpty()) {
			return false; // there is no player
		}
		return objects.get(posOpt.get()).stream() //
				.anyMatch(GameObjectConfig::isExit);
	}

	/**
	 * Returns all objects in the current level.
	 */
	public Map<Position, List<GameObjectConfig>> getGameObjects() {
		return objects;
	}

	/**
	 * Returns the codes of the locks that are currently open.
	 */
	public Set<String> getOpenLocks() {
		return openLocks;
	}

	public int getLevelWidth() {
		return levelWidth;
	}

	public int getLevelHeight() {
		return levelHeight;
	}

}
