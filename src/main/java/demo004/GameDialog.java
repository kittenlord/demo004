package demo004;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import demo004.config.Config;
import demo004.config.GameObjectConfig;

/**
 * The game window, its painting, and key events.
 */
public class GameDialog {

	// key = KeyEvent.VK_xyz constant
	private static final Map<Integer, Direction> KEY_DIRECTIONS;

	private final Config cfg;
	private GameState state;
	private BufferedImage titleImage;
	private Map<GameObjectConfig, BufferedImage> objectImages = new HashMap<>();

	private JFrame frame;
	private BufferedImage canvasBuffer;

	static {
		Map<Integer, Direction> kd = new HashMap<>();
		kd.put(KeyEvent.VK_UP, Direction.UP);
		kd.put(KeyEvent.VK_KP_UP, Direction.UP);
		kd.put(KeyEvent.VK_RIGHT, Direction.RIGHT);
		kd.put(KeyEvent.VK_KP_RIGHT, Direction.RIGHT);
		kd.put(KeyEvent.VK_DOWN, Direction.DOWN);
		kd.put(KeyEvent.VK_KP_DOWN, Direction.DOWN);
		kd.put(KeyEvent.VK_LEFT, Direction.LEFT);
		kd.put(KeyEvent.VK_KP_LEFT, Direction.LEFT);
		KEY_DIRECTIONS = Collections.unmodifiableMap(kd);
	}

	public GameDialog(Config config) {
		cfg = config;
		state = new GameState(config);
		titleImage = Resources.getImage(cfg.titleImage());
		for (var objCfg : config.objects()) {
			objectImages.put(objCfg, Resources.whiteToTransparent(Resources.getImage(objCfg.image())));
		}
	}

	public void create() {
		var canvas = createCanvas();
		frame = new JFrame(cfg.windowName());
		frame.setIconImage(Resources.getImage(cfg.windowIcon()));
		frame.add(canvas); 
		frame.pack();
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
		canvas.requestFocusInWindow();
	}

	private Canvas createCanvas() {
		canvasBuffer = new BufferedImage(cfg.windowWidth(), cfg.windowHeight(), BufferedImage.TYPE_INT_ARGB);
		var canvas = new Canvas() {

			private static final long serialVersionUID = 1L;

			/**
			 * Paint to off-screen buffer, to prevent flicker.
			 */
			@Override
			public void paint(Graphics g) {
				paintToBuffer(canvasBuffer.getGraphics());
				g.drawImage(canvasBuffer, 0, 0, null);
			}

			private void paintToBuffer(Graphics g) {
				if (null == state.currentLevel) {
					g.drawImage(titleImage, 0, 0, null);
				} else {
					g.setColor(Color.decode(cfg.levels().get(state.currentLevel).backgroundColor()));
					g.fillRect(0, 0, cfg.windowWidth(), cfg.windowHeight());
					int w = cfg.objectWidth();
					int h = cfg.objectHeight();
					int x0 = (cfg.windowWidth() - state.getLevelWidth() * w) / 2;
					int y0 = (cfg.windowHeight() - state.getLevelHeight() * h) / 2;
					for (var objEntry : state.getGameObjects().entrySet()) {
						var pos = objEntry.getKey();
						var x = x0 + pos.x() * w;
						var y = y0 + pos.y() * h;
						for (var obj : objEntry.getValue()) {
							var lock = obj.lockCode();
							if ((null != lock) && state.getOpenLocks().contains(lock)) {
								// do not paint locks that are already open
							} else {
								g.drawImage(objectImages.get(obj), x, y, null);
							}
						}
					}
				}
			}

			@Override
			public void update(Graphics g) {
				paint(g);
			}

		};
		canvas.setPreferredSize(new Dimension(cfg.windowWidth(), cfg.windowHeight()));
		canvas.addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (null == state.currentLevel) {
					if (KeyEvent.VK_SPACE == e.getKeyCode()) {
						try {
							state.initLevel(0);
						} catch (IOException ex) {
							state.currentLevel = null;
							JOptionPane.showMessageDialog(frame, "Error", ex.getMessage(), JOptionPane.ERROR_MESSAGE);
						}
						canvas.repaint();
					}
				} else {
					var direction = KEY_DIRECTIONS.get(e.getKeyCode());
					if (null != direction) {
						state.movePlayer(direction);
						if (state.isLevelCompleted()) {
							try {
								state.nextLevel();
							} catch (IOException ex) {
								state.currentLevel = null;
								JOptionPane.showMessageDialog(frame, "Error", ex.getMessage(),
										JOptionPane.ERROR_MESSAGE);
							}
						}
						canvas.repaint();
					}
				}
			}

		});
		return canvas;
	}

}
