package demo004;

/**
 * Four basic directions on the screen.
 */
public enum Direction {
	UP(0, -1), RIGHT(1, 0), DOWN(0, 1), LEFT(-1, 0);
	
	private final int dx;
	private final int dy;
	
	private Direction(int dx, int dy) {
		this.dx = dx;
		this.dy = dy;
	}

	/**
	 * The next position from the given position in this direction.
	 */
	public Position nextPosition(Position pos) {
		return new Position(pos.x() + dx, pos.y() + dy);
	}
	
}
