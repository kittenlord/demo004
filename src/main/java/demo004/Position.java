package demo004;

/**
 * Position on the screen.
 */
public record Position(int x, int y) {
	//
}
