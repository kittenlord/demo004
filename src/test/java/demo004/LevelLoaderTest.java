package demo004;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import demo004.config.GameObjectConfig;

class LevelLoaderTest {

	private static LevelLoader loader;

	@BeforeAll
	static void setup() {
		loader = new LevelLoader(Arrays.asList( //
				mockObject("a"), //
				mockObject("x1"), //
				mockObject("x2") //
		));
	}

	@Test
	void testLoad() {
		var map = loader.loadLevel(Arrays.asList(//
				"a x1 a  a", //
				"  a  x2   x1aa" //
		));

		// decode single character
		var group1 = map.get(new Position(0, 0));
		assertThat(group1).hasSize(1);
		assertThat(group1.get(0).mapCode()).isEqualTo("a");

		// decode multiple characters but single object
		var group2 = map.get(new Position(1, 0));
		assertThat(group2).hasSize(1);
		assertThat(group2.get(0).mapCode()).isEqualTo("x1");

		assertThat(map.get(new Position(2, 0)).get(0).mapCode()).isEqualTo("a");
		assertThat(map.get(new Position(3, 0)).get(0).mapCode()).isEqualTo("a");

		// shorter line end 
		assertThat(map.get(new Position(4, 0))).isNull();

		// skipped position at beginning
		assertThat(map.get(new Position(0, 1))).isNull();

		assertThat(map.get(new Position(1, 1)).get(0).mapCode()).isEqualTo("a");
		assertThat(map.get(new Position(2, 1)).get(0).mapCode()).isEqualTo("x2");

		// skipped position at middle
		assertThat(map.get(new Position(3, 1))).isNull();

		// decode multiple objects
		var group3 = map.get(new Position(4, 1));
		assertThat(group3).hasSize(3);
		assertThat(group3.get(0).mapCode()).isEqualTo("x1");
		assertThat(group3.get(1).mapCode()).isEqualTo("a");
		assertThat(group3.get(2).mapCode()).isEqualTo("a");
	}

	private static GameObjectConfig mockObject(String code) {
		return new GameObjectConfig(code, "mock.png", false, false, false, null, null);
	}

}
