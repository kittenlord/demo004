package demo004;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class DirectionTest {

	@Test
	void testNextPosition() {
		var p = new Position(15, 25);
		assertThat(Direction.LEFT.nextPosition(p)).isEqualTo(new Position(14, 25));
		assertThat(Direction.RIGHT.nextPosition(p)).isEqualTo(new Position(16, 25));
		assertThat(Direction.UP.nextPosition(p)).isEqualTo(new Position(15, 24));
		assertThat(Direction.DOWN.nextPosition(p)).isEqualTo(new Position(15, 26));
	}

}
