package demo004.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class ConfigLoaderTest {

	public record BigRecord(String name, int value, boolean active, List<SmallRecord> small) {
		//
	}

	public record SmallRecord(String name, int value, boolean active) {
		//
	}

	static private ConfigLoader loader;

	@BeforeAll
	static void setup() throws IOException {
		var content = String.join("\n", //
				"# example data", //
				"", //
				"name=Big", //
				"value=123", //
				"active=true", //
				"", //
				"small.1.name=Small 1", //
				"small.1.value=1", //
				"small.1.active=true", //
				"", //
				"small.2.name=Small 2", //
				"small.2.value=2", //
				"small.2.active=false", //
				"", //
				"small.3.name=Small 3", //
				"small.3.value=3");
		Properties props = new Properties();
		props.load(new ByteArrayInputStream(content.getBytes()));
		loader = new ConfigLoader(props);
	}

	@Test
	void testLoad() {
		BigRecord big = loader.load(BigRecord.class);
		assertThat(big.name()).isEqualTo("Big");
		assertThat(big.value()).isEqualTo(123);
		assertThat(big.active()).isTrue();

		SmallRecord small1 = big.small().get(0);
		assertThat(small1.name()).isEqualTo("Small 1");
		assertThat(small1.value()).isEqualTo(1);
		assertThat(small1.active()).isTrue();

		SmallRecord small2 = big.small().get(1);
		assertThat(small2.name()).isEqualTo("Small 2");
		assertThat(small2.value()).isEqualTo(2);
		assertThat(small2.active()).isFalse();

		SmallRecord small3 = big.small().get(2);
		assertThat(small3.name()).isEqualTo("Small 3");
		assertThat(small3.value()).isEqualTo(3);
		assertThat(small3.active()).isFalse();
	}

}
