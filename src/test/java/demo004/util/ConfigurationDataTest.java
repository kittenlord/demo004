package demo004.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Properties;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class ConfigurationDataTest {

	static private ConfigurationData data;

	@BeforeAll
	static void setup() throws IOException {
		var content = String.join("\n", //
				"# example data", //
				"", //
				"stringValue=Hello World", //
				"", //
				"intValue=123456789", //
				"intValueHex=#face", //
				"", //
				"boolValueTrue=yes", //
				"boolValueFalse=no", //
				"", //
				"arrayValue.1.name=one", //
				"arrayValue.2.name=two", //
				"arrayValue.3.name=three");
		Properties props = new Properties();
		props.load(new ByteArrayInputStream(content.getBytes()));
		data = new ConfigurationData(props);
	}

	@Test
	void testGetString() {
		assertThat(data.getString("stringValue")).isEqualTo("Hello World");
		assertThat(data.getString("stringValueMissing")).isNull();
	}

	@Test
	void testGetInt() {
		assertThat(data.getInt("intValue")).isEqualTo(123456789);
		assertThat(data.getInt("intValueHex")).isEqualTo(0xface);
		Assertions.assertThrows(RuntimeException.class, () -> data.getInt("intValueMissing"));
		Assertions.assertThrows(RuntimeException.class, () -> data.getInt("stringValue"));
	}

	@Test
	void testGetBool() {
		assertThat(data.getBool("boolValueTrue")).isTrue();
		assertThat(data.getBool("boolValueFalse")).isFalse();
		assertThat(data.getBool("boolValueMissing")).isFalse();
		Assertions.assertThrows(RuntimeException.class, () -> data.getBool("stringValue"));
	}

	@Test
	void testGetSubKeys() {
		assertThat(data.getSubKeysAsStream("arrayValue")).containsExactly("1", "2", "3");
	}

}
